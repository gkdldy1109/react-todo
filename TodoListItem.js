import { Component } from "react";
import { render } from "react-dom";

const style = {
    display: 'none'
}

export default class TodoListItem extends Component {


    itemCheck = () => {
        const checkComponent = document.querySelector("#todo-item" + this.props.id);
        if (checkComponent.style.cssText) {
            checkComponent.style.cssText = 0;
        } else {
            checkComponent.style.cssText = "text-decoration: line-through";
        }
    }

    modify = () => {
        const modifyInpt = document.querySelector("#todo-modify-inpt" + this.props.id);
        const modifyBtn = document.querySelector("#todo-modify-btn" + this.props.id);
        if (modifyInpt.style.display === 'none' && modifyBtn.style.display === 'none') {
            modifyInpt.style.display = 'block';
            modifyBtn.style.display = 'block';
        } else {
            modifyInpt.style.display = 'none';
            modifyBtn.style.display = 'none';
        }
    }

    render() {
        return (
            <li key={this.props.id.toString()} id={"todo-item" + this.props.id}>
                <input type="button" value="✅" onClick={this.itemCheck}></input>
                {this.props.text}
                <input type="text" id={"todo-modify-inpt" + this.props.id} style={style}></input>
                <input type="button" id={"todo-modify-btn" + this.props.id} value="수정완료" onClick={() => { this.props.modifyFin(this.props.id) }} style={style} />
                <input type="button" value="삭제" onClick={() => { this.props.delete(this.props.id) }} />
                <input type="button" value="수정" onClick={this.modify} />
            </li>
        )
    }
}