import React, { Component } from 'react';
import Title from './Title';
import Content from './Content';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Title text="Todo List" />
        <Content />
      </div>

    );
  }
}

export default App;
