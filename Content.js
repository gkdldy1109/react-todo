import { Component } from "react";
import TodoList from './TodoList';
import TodoListItem from "./TodoListItem";


export default class Content extends Component {
    //constructor 사용시 주의!
    //this.setState로 초기값 할당 X
    //this.state로 초기값 할당 O
    constructor(props) {
        super(props);
        this.state = {
            itemNum: 0,
            items: []
        }
    }

    addItmes = () => {
        const inputText = document.querySelector('#inputText');

        //이때 주의해야 할 점은 this.state.items에 바로 push 하면 안 되고, 
        //배열을 복사([...배열]방법)한 후에 복사한 배열에 push를 해야 한다. 
        //그러고 나서 복사한 배열을 setState로 변경해줘야 한다.
        if (inputText.value) {
            const tempArr = [...this.state.items]; //배열 복사방법 [...배열명]
            // tempArr.push(<li key={inputText.value}>{inputText.value}</li>); //li에 key를 설정해주는게 좋음

            tempArr.push(<TodoListItem
                id={this.state.itemNum++}
                text={inputText.value}
                delete={(num) => this.deleteItem(num)}
                modifyFin={(num) => this.modifyItem(num)}
            ></TodoListItem>)
            this.setState({
                items: tempArr
            })
            inputText.value = "";
        }
    };


    deleteItem(num) {
        const selected = document.querySelector('#todo-item' + num);
        if (selected) {
            selected.remove();
        }
    }

    modifyItem(num) {
        const changeTxt = document.querySelector('#todo-modify-inpt' + num);
        if (changeTxt.value) {

            const tempArr = this.state.items; //배열 복사방법 [...배열명]
            console.log(tempArr)

            for (let i = 0; i < tempArr.length; i++) {
                if (i == num) {
                    tempArr[num] = <TodoListItem
                        id={num}
                        text={changeTxt.value}
                        delete={(num) => this.deleteItem(num)}
                        modifyFin={(num) => this.modifyItem(num)}
                    ></TodoListItem>;
                }
            }

            this.setState({
                items: tempArr
            })

            const modifyInpt = document.querySelector("#todo-modify-inpt" + num);
            const modifyBtn = document.querySelector("#todo-modify-btn" + num);
            modifyInpt.value = '';
            modifyInpt.style.display = 'none';
            modifyBtn.style.display = 'none';
        }
    }

    render() {
        return (
            <div>
                <input id="inputText" type="text" placeholder="입력"></input>
                <input type="button" value="↩" onClick={this.addItmes}></input>
                <TodoList items={this.state.items} />
            </div>
        )
    }
}